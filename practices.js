const serachItem = document.querySelector(".search-box");
const loader = document.querySelector(".loader");

loader.style.display = 'block';

fetch('https://fakestoreapi.com/products')
    .then(res => res.json())
    .then(json => {
        loader.style.display = 'none';
        console.log(json.length)
        if (json.length == 0) {

            // API loaded successfully, but no products found
            document.querySelector(".main-container").innerHTML = "<h1>No Product found!</h1>"
        }
        Object.entries(json).map((item) => {

            return document.querySelector(".main-container").innerHTML += `<div class="cart">
            
                    <img  class="img"src="${item[1].image}"/>
                    <div class="title"> ${item[1].title}</div>
                    <div class="price"> price:${item[1].price}</div>
                    <div class="rate-id">
                    <div class="rating">${item[1].rating.rate}<i class="fa-sharp fa-solid fa-star"></i></div>
                    </div>
                    <div class="btn">
                    <button>Buy now</button>
                    <button class="add-cart">Add to cart</button>
                    </div>           
                    
            </div>`



        })


    }).catch((err) => {

        document.querySelector(".main-container").innerHTML= "<h1> Sorry some error occured!</h1>";

    })

serachItem.addEventListener('change', () => {
    console.log(serachItem.value)
    loader.style.display = 'block';
    document.querySelector(".main-container").innerHTML = null;

    if (serachItem.value != 'all') {
        fetch(`https://fakestoreapi.com/products/category/${serachItem.value}`)

            .then(res => res.json())
            .then(json => {
                loader.style.display = 'none';
                console.log(json)
                if (json.length == 0) {

                    // API loaded successfully, but no products found
                    document.querySelector(".main-container").innerHTML = "<h1>No Product found!</h1>"
                }

                json.map((item) => {

                    return document.querySelector(".main-container").innerHTML += `<div class="cart">
            
                    <img  class="img"src="${item.image}"/>
                    <div class="title"> ${item.title}</div>
                    <div class="price"> price:${item.price}</div>
                    <div class="rate-id">
                    <div class="rating">${item.rating.rate}<i class="fa-sharp fa-solid fa-star"></i></div>
                    </div>
                    <div class="btn">
                    <button>Buy now</button>
                    <button class="add-cart">Add to cart</button>
                    </div>           
                    
            </div>`



                })


            })
            .catch((err) => {

                document.querySelector(".main-container").innerHTML = "<h1>some error occured!</h1>";

            })




    } else {
        window.location.reload();
    }
})
