document.getElementById('signup-form').addEventListener('submit', function (event) {
    let firstName = document.getElementById('firstName').value;
    let lastName = document.getElementById('lastName').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let confirmPassword = document.getElementById('confirmPassword').value;
    let tos = document.getElementById('tos').checked;
    let errorMessagesBox = [];

    if (!firstName) {
        errorMessagesBox.push("First Name is required");
    } else if ((!/^[a-zA-Z]+$/.test(firstName)) || firstName.length<3) {
        errorMessagesBox.push("Invalid input for First Name");
    }

    if (!lastName) {
        errorMessagesBox.push("Last Name is required");
    } else if ((!/^[a-zA-Z]+$/.test(lastName)) || lastName.length<3) {
        errorMessagesBox.push("Invalid input for Last Name");
    }

    if (!email) {
        errorMessagesBox.push("Email Address is required");
    }else if (!email.includes("@gmail.com")) {
        errorMessagesBox.push("Email required @gmail.com");
    }

    if (!password) {
        errorMessagesBox.push("Password is required");
    }else if (password.length<3) {
        errorMessagesBox.push("please add strong password");
    }

    if (password !== confirmPassword) {
        errorMessagesBox.push("Passwords do not match");
    }

    if (!tos) {
        errorMessagesBox.push("You must agree to the Terms of Service");
    }

    if (errorMessagesBox.length > 0) {
        event.preventDefault();
        let errorMessageElement = document.createElement('div');
        errorMessageElement.className = 'error-message';
        errorMessageElement.innerHTML = errorMessagesBox.join('<br>');
        document.getElementById('signup-form').appendChild(errorMessageElement);
        setTimeout(() => {
            const child = document.getElementById('signup-form').children[7];
            console.log(child);
            document.getElementById('signup-form').removeChild(child);
        }, 5000);
    } else {
        event.preventDefault();
        window.location.href = "index.html";
    }
});
