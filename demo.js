const searchItem = document.querySelector(".search-box");
const mainContainer = document.querySelector(".main-container");
const sign_up=document.querySelector(".Sign-up");

fetch('https://fakestoreapi.com/products')
  .then(res => res.json())
  .then(json => {
    console.log(json);
    json.map(item => {
      const cart = document.createElement("div");
      cart.classList.add("cart");

      const img = document.createElement("img");
      img.classList.add("img");
      img.src = item.image;
      cart.appendChild(img);

      const title = document.createElement("div");
      title.classList.add("title");
      title.innerText = item.title;
      cart.appendChild(title);

      const price = document.createElement("div");
      price.classList.add("price");
      price.innerText = `Price: ${item.price}`;
      cart.appendChild(price);

      const btn = document.createElement("div");
      btn.classList.add("btn");

      const buyNowBtn = document.createElement("button");
      buyNowBtn.innerText = "Buy now";
      btn.appendChild(buyNowBtn);

      const addCartBtn = document.createElement("button");
      addCartBtn.classList.add("add-cart");
      addCartBtn.innerText = "Add to cart";
      btn.appendChild(addCartBtn);

      cart.appendChild(btn);
      mainContainer.appendChild(cart);
    });
  });

searchItem.addEventListener('change', () => {
  console.log(searchItem.value);
  if (searchItem.value !== 'all') {
    fetch(`https://fakestoreapi.com/products/category/${searchItem.value}`)
      .then(res => res.json())
      .then(json => {
        console.log(json);
        mainContainer.innerHTML = '';
        json.map(item => {
          const cart = document.createElement("div");
          cart.classList.add("cart");

          const img = document.createElement("img");
          img.classList.add("img");
          img.src = item.image;
          cart.appendChild(img);

          const title = document.createElement("div");
          title.classList.add("title");
          title.innerText = item.title;
          cart.appendChild(title);

          const price = document.createElement("div");
          price.classList.add("price");
          price.innerText = `Price: ${item.price}`;
          cart.appendChild(price);

          const btn = document.createElement("div");
          btn.classList.add("btn");

          const buyNowBtn = document.createElement("button");
          buyNowBtn.innerText = "Buy now";
          btn.appendChild(buyNowBtn);

          const addCartBtn = document.createElement("button");
          addCartBtn.classList.add("add-cart");
          addCartBtn.innerText = "Add to cart";
          btn.appendChild(addCartBtn);

          cart.appendChild(btn);
          mainContainer.appendChild(cart);
        });
      });
  } else {
    window.location.reload();
  }
});

